﻿using AutoMapper;
using Data;
using Services.Contracts;
using Services.Services;
using System;
using System.Threading.Tasks;

namespace Services
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly FastScoreContext context;
        private readonly IMapper mapper;
        private RoleService roleService;
        private AccountService accountService;
        private SportsService sportsService;
        private MatchesService matchesService;
        private FirstHalfsService firstHalfsService;
        private SecondHalfsService secondHalfsService;

        public UnitOfWork(FastScoreContext context, IMapper mapper)
        {
            this.context = context;
            this.mapper = mapper;
        }

        public FirstHalfsService FirstHalfsService
        {
            get
            {
                if(this.firstHalfsService == null)
                {
                    this.firstHalfsService = new FirstHalfsService(context, this);
                }

                return this.firstHalfsService;
            }
        }
        
        public SecondHalfsService SecondHalfsService
        {
            get
            {
                if(this.secondHalfsService == null)
                {
                    this.secondHalfsService = new SecondHalfsService(context, this);
                }

                return this.secondHalfsService;
            }
        }

        public RoleService RoleService
        {
            get
            {
                if (this.roleService == null)
                {
                    this.roleService = new RoleService(this.context);
                }

                return this.roleService;
            }
        }

        public AccountService AccountService
        {
            get
            {
                if (this.accountService == null)
                {
                    this.accountService = new AccountService(this.context);
                }

                return this.accountService;
            }
        }

        public SportsService SportsService
        {
            get
            {
                if (this.sportsService == null)
                {
                    this.sportsService = new SportsService(this.context);
                }

                return this.sportsService;
            }
        }

        public MatchesService MatchesService
        {
            get
            {
                if (this.matchesService == null)
                {
                    this.matchesService = new MatchesService(this.context, this.mapper);
                }

                return this.matchesService;
            }
        }

        public async Task SaveChangesAsync()
        {
            try
            {
                await this.context.SaveChangesAsync();
            }
            catch(Exception e)
            {
                throw new InvalidOperationException(e.InnerException.Message);
            }
        }
    }
}
