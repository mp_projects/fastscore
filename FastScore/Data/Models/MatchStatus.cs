﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Data.Models
{
    public class MatchStatus
    {
        [Key]
        public int Id { get; set; }

        public string Name { get; set; }

        public IList<Match> Matches { get; set; }
    }
}
