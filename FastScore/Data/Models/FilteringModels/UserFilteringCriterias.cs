﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Models.FilteringModels
{
    public class UserFilteringCriterias
    {
        public string UserName { get; set; }

        public string Email { get; set; }
    }
}
