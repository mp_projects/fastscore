﻿using Data.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Data.ModelsConfig
{
    class FirstHalfConfigs : IEntityTypeConfiguration<FirstHalf>
    {
        public void Configure(EntityTypeBuilder<FirstHalf> builder)
        {
            builder.HasMany(fh => fh.HomeGoals)
                .WithOne(g => g.FirstHalfHomeGoals)
                .HasForeignKey(g => g.FirstHalfIdHomeGoals);

            builder.HasMany(fh => fh.AwayGoals)
                .WithOne(g => g.FirstHalfAwayGoals)
                .HasForeignKey(g => g.FirstHalfIdAwayGoals);
        }
    }
}
