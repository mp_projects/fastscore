﻿using Data.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Data.ModelsConfig
{
    class SecondHalfConfigs : IEntityTypeConfiguration<SecondHalf>
    {
        public void Configure(EntityTypeBuilder<SecondHalf> builder)
        {
            builder.HasMany(fh => fh.HomeGoals)
              .WithOne(g => g.SecondHalfHomeGoals)
              .HasForeignKey(g => g.SecondHalfIdHomeGoals);

            builder.HasMany(fh => fh.AwayGoals)
                .WithOne(g => g.SecondHalfAwayGoals)
                .HasForeignKey(g => g.SecondHalfIdAwayGoals);
        }
    }
}
