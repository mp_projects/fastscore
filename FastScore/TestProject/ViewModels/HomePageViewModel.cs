﻿using Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestProject.ViewModels
{
    public class HomePageViewModel
    {
        public IList<Sport> Sports = new List<Sport>();
    }
}
