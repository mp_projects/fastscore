﻿using Consts;
using Data;
using Data.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestProject.ViewModels.RoleVMs;

namespace TestProject.Controllers
{
    //[Authorize(Roles = UserRoleConsts.ADMIN)]
    public class AdministrationController : Controller
    {
        public IActionResult Manage()
        {
            return View();
        }
    }
}
