﻿using Data;
using Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestProject.ViewModels;

namespace TestProject.Mapping
{
    public static class ViewModelMapper
    {
        public static IDictionary<LeagueViewModel, IEnumerable<MatchViewModel>> Map(IDictionary<League, IEnumerable<Match>> leaguesMatches)
        {
            if (leaguesMatches == null)
            {
                throw new NullReferenceException();
            }

            var dic = new Dictionary<LeagueViewModel, IEnumerable<MatchViewModel>>();

            foreach (var kvp in leaguesMatches)
            {
                var leagueVM = new LeagueViewModel()
                {
                    LeagueId = kvp.Key.Id,
                    LeagueSportId = kvp.Key.SportId,
                    LeagueName = kvp.Key.Name,
                    LeagueCountryName = kvp.Key.Country.Name,
                    LeagueCountryFlagPhotoUrl = kvp.Key.Country.FlagPhotoUrl
                };
                var matchesVM = new List<MatchViewModel>();

                foreach (var match in kvp.Value)
                {
                    var matchVM = new MatchViewModel()
                    {
                        MatchId = match.Id,
                        MatchStatusId = match.MatchStatusId,
                        StartingTime = match.StartingTime.ToString("HH:mm"),
                        HasPreview = match.HasPreview,
                        AwayTeamFlagPhotoUrl = match.AwayTeam.FlagPhotoUrl,
                        AwayTeamName = match.AwayTeam.Name,
                        HomeTeamFlagPhotoUrl = match.HomeTeam.FlagPhotoUrl,
                        HomeTeamName = match.HomeTeam.Name,
                    };

                    try
                    {
                        matchVM.FirstHalfCurrMinute = match.FirstHalf.CurrMinute;
                        matchVM.FirstHalfAwayGoalsCount = match.FirstHalf.AwayGoals.Count();
                        matchVM.FirstHalfHomeGoalsCount = match.FirstHalf.HomeGoals.Count();

                        try
                        {
                            matchVM.FirstHalfCurrMinute = match.FirstHalf.CurrMinute;
                            matchVM.FirstHalfAwayGoalsCount = match.FirstHalf.AwayGoals.Count();
                            matchVM.FirstHalfHomeGoalsCount = match.FirstHalf.HomeGoals.Count();
                        }
                        catch (NullReferenceException)
                        {
                            //???
                        }
                    }
                    catch (NullReferenceException)
                    {
                        //???
                    }

                    matchesVM.Add(matchVM);
                }

                dic.Add(leagueVM, matchesVM);
            }

            return dic;
        }
    }
}
